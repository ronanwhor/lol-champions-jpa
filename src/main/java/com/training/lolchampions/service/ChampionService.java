package com.training.lolchampions.service;

import com.training.lolchampions.entities.LolChampion;
import com.training.lolchampions.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChampionService {

    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<LolChampion> findAll(){
        return lolChampionRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion){}

}
