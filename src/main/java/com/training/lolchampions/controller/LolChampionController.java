package com.training.lolchampions.controller;


import com.training.lolchampions.entities.LolChampion;
import com.training.lolchampions.service.ChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class LolChampionController {

    @Autowired
    private ChampionService championService;

    @GetMapping
    public List<LolChampion> findAll(){
        return championService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion){}

}
